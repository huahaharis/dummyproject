import React from 'react'
import {View, Text, Image, StyleSheet, TouchableOpacity, TextInput, FlatList, Modal} from 'react-native';
import { Container, Header, Left, Body, Right, Button, Thumbnail, Card, CardItem, Content } from 'native-base';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Entypo';
import Icon2 from 'react-native-vector-icons/AntDesign';
import {getKey} from '../../utils/shared-pref';
import axios from 'axios';

const ListMateri = () => {
    const navigation = useNavigation()
    const [datas, setDatas]= React.useState([])

    React.useEffect(async()=>{
      axios
        .get(`https://gorest.co.in/public/v1/users`,{
          headers: {
            Authorization: `Bearer ${'8db27eb8870a5e37aea13e93d681e03e4a00bf70ed1fdeb672063dde80c3e905'}`
          }
        })
        .then(response => {
          setDatas(response.data.data)
          console.log(JSON.stringify(response.data.data,null,2));
        })
        .catch(err => {
          console.log(err);
        });
    },[])


    return (
      <View style={styles.container}>
        <View>
          <Text style={styles.h2text}>List User</Text>
        </View>
        <FlatList
          data={datas}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => {
            return (
              <View style={{width: 400}}>
                <Card>
                  <CardItem>
                    <Left>
                      <Body>
                        <Text>Name: {item.name}</Text>
                        <Text>Gender: {item.gender}</Text>
                        <Text>Email: {item.email}</Text>
                        <Text>Status USer: {item.status}</Text>
                      </Body>
                    </Left>
                  </CardItem>
                </Card>
              </View>
            );
          }}
        />
      </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    h2text: {
        marginTop: 10,
        fontFamily: 'Helvetica',
        fontSize: 36,
        fontWeight: 'bold',
    },
    flatview: {
        justifyContent: 'center',
        alignItems: "center",
        paddingTop: 30,
        borderRadius: 2,
    },
    name: {
        fontFamily: 'Verdana',
        fontSize: 18,
        alignItems: "center"
    },
    floatingbutton: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 60,
        position: "absolute",
        bottom: 10,
        right: 10,
        height: 60,
        backgroundColor: '#fff',
        borderRadius: 60,
    }, wrapper: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        height: "100%",
        width: "100%",
        zIndex: 9999
    },
    blurView: {
        flex: 1,
        zIndex: 99999
    },
    containerbaru: {
        margin: 25,
        zIndex: 99999,
        position: "relative",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    box: {
        backgroundColor: "white",
        padding: 20,
        position: "absolute",
        top: "15%",
        borderColor: "black",
        borderWidth: StyleSheet.hairlineWidth,
        margin: 25,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
        zIndex: 99999,
        width: "85%"
    },
    titleText: {
        fontFamily: "Lora",
        fontSize: 20
    },
    bodyText: {
        marginTop: 15,
        textAlign: "center",
        fontFamily: "Lora",
        fontSize: 16,
        lineHeight: 20
    },
    modalView: {
        margin: 10,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 80,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 10
    },
});

export default ListMateri