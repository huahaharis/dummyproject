import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Dimensions,
  Image,
  StatusBar,
  ToastAndroid,
  ScrollView,
  Alert,
} from 'react-native';
import * as actions from '../../redux/actions/index';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const CreateUser = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [name, setName] = React.useState('');
  const [email, setEmail] = React.useState('');
  const [gender, setGender] = React.useState('');
  const [status, setStatus] = React.useState('');
  const redux = useSelector(state => state);

  const inputName = () => {
    dispatch(actions.inputName(name, email, gender, status));
  };
  React.useEffect(() => {
navigation.navigate('lihatmateri');
  }, [redux?.CreateReducers]);
  return (
    <View style={style.container}>
      <StatusBar hidden />
      <ImageBackground
        source={require('../../components/bg.jpeg')}
        style={{width: windowWidth, height: '60%'}}></ImageBackground>
      <View style={style.halfUp}>
        <TextInput
          style={style.inputField}
          onChangeText={text => setName(text)}
          value={name}
          editable={true}
          maxLength={40}
          multiline={false}
          placeholder="Name"
        />
        <TextInput
          onChangeText={text => setEmail(text)}
          value={email}
          style={style.inputField2}
          editable={true}
          maxLength={40}
          multiline={false}
          placeholder="Email"
        />
        <TextInput
          onChangeText={text => setGender(text)}
          value={gender}
          style={style.inputField2}
          editable={true}
          maxLength={40}
          multiline={false}
          placeholder="Gender"
        />
        <TextInput
          onChangeText={text => setStatus(text)}
          value={status}
          style={style.inputField2}
          editable={true}
          maxLength={40}
          multiline={false}
          placeholder="Status"
        />
        <TouchableOpacity style={style.button} onPress={inputName}>
          <Text
            style={{
              alignSelf: 'center',
              top: 2,
              fontWeight: 'bold',
            }}>
            Submit
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  halfUp: {
    backgroundColor: '#F1E9E7',
    elevation: 10,
    shadowOpacity: 4,
    width: windowWidth / 1.5,
    height: '50%',
    borderRadius: 15 / 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: '16.2%',
    marginTop: '-49%',
  },
  inputField: {
    width: '82%',
    height: '15%',
    fontSize: 14,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
  },
  inputField2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '82%',
    height: '15%',
    fontSize: 14,
    borderRadius: 10,
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginTop: '3%',
  },
  button: {
    width: windowWidth * 0.5,
    height: windowHeight * 0.04,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#ffffff',
    marginTop: 30,
  },
  button2: {
    width: '32%',
    height: '65%',
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#F99C5E',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  searchSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    width: '75%',
    height: 40,
    marginHorizontal: '7.5%',
    borderRadius: 15,
    margin: 10,
    paddingLeft: 2,
    paddingRight: 16,
  },
});

export default CreateUser;
