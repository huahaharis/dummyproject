import Types from '../../../utils/Types';
import axios from 'axios'

export const inputName = (name, email, gender, status)=>{
    // console.log(name);
    return(dispatch)=>{
        axios
          .post('https://gorest.co.in/public/v1/users', {
            'name': name,
            'email': email,
            'gender': gender,
            'status': status,
          },{
            headers: {
              Authorization: `Bearer ${'8db27eb8870a5e37aea13e93d681e03e4a00bf70ed1fdeb672063dde80c3e905'}`
            }
          })
          .then(result => {
            console.log('RES Create User ', JSON.stringify(result, null, 2));
            dispatch(createUserSuccess(result.data));
          })
          .catch(error => {
            console.log('error Create ', JSON.stringify(error, null, 2));
            dispatch(createUserFailed(error.message));
          });
    }
}

const createUserSuccess =(data)=>{
    return {
        type: Types.CREATE_SUCCESS,
        data
      }
}

const createUserFailed =(data)=>{
  return {
      type: Types.CREATE_FAIL,
      data
    }
}