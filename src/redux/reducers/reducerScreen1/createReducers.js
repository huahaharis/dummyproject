import Types from '../../../utils/Types';

const INITIAL_STATE = {
  Response: null,
  Error: null
};

const CreateReducers = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Types.CREATE_SUCCESS:
      state = {
        ...state,
        Response: action.data,
        Error: null
      };
      break;
    case Types.CREATE_FAIL:
      state = {
        ...state,
        Response: null,
        Error: action.data,
      };
      break;
    default:
      break;
  }

  return state;
};

export default CreateReducers;
