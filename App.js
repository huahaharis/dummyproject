import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import CreateUser from './src/screen/createUser';
import ListMateri from './src/screen/belajar/listMateri';

const stack = createStackNavigator();

const App =()=>{
  return(
  <NavigationContainer>
    <stack.Navigator>
      <stack.Screen name="createuser" component={CreateUser} options={{headerShown:false}}/>
      <stack.Screen name="lihatmateri" component={ListMateri} options={{headerShown:false}}/>
    </stack.Navigator>
  </NavigationContainer>
  )
}

export default App;